import { RecipeData } from './model'
import { Nutrients } from './Nutrients'
import './Recipe.css'
import { ClockIcon } from './svg/ClockIcon'

export type RecipeProps = {
  recipe: RecipeData
}

const labels = [
  'Knaller',
  'Highlight',
  'Brainfood',
  'Lieblingsessen',
  'Junk Food',
  'Fast Food',
  'Poison',
]

export const Recipe = ({ recipe }: RecipeProps) => {
  const label = labels[Math.floor(Math.random() * labels.length)]

  return (
    <div className="Recipe">
      <div className="Recipe-Image">
        <img src={recipe.image} alt="" />
        <div className="Recipe-Label">{label}</div>
        <div className="Recipe-Title">{recipe.title}</div>
        <div className="Recipe-Duration">
          <ClockIcon /> {recipe.subtitle}
        </div>
      </div>
      <div className="Recipe-Nutrients">
        Anteile Gesamtbedarf
        <Nutrients {...recipe.nutrients} />
      </div>
    </div>
  )
}
