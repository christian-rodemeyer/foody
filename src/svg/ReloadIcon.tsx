export const ReloadIcon = () => (
  <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M4 9.75H1M1 9.75C1 9.75 3.5 13.25 7.25 13.25C10.702 13.25 13.5 10.75 13.5 7.75M1 9.75V13.25M10.5 4.25H13.5M13.5 4.25C13.5 4.25 11 0.75 7.25 0.75C3.798 0.75 1 3.25 1 6.25M13.5 4.25V0.75"
      stroke="#2A49FF"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)
