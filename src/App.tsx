import './App.css'
import { DailyPlan } from './DailyPlan'
import { MenuIcon } from './svg/MenuIcon'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MenuIcon className="App-header-menu" />
        <h1 className="App-header-title">Foody</h1>
        <div className="App-header-login"></div>
      </header>
      <DailyPlan />
      <footer className="App-footer">
        <ul>
          <li>Home</li>
          <li>About</li>
          <li>FAQ</li>
          <li>Impressum</li>
        </ul>
      </footer>
    </div>
  )
}

export default App
