import './DailyPlan.css'
import { Recipe } from './Recipe'
import { Nutrients } from './Nutrients'
import { Meal } from './Meal'
import { format } from 'date-fns'
import { de } from 'date-fns/locale'
import { getRecipe } from './model'

const locale = { locale: de }

const random = () => Math.floor(Math.random() * 90) + 10

export const DailyPlan = () => {
  const now = new Date()
  const day = format(now, 'eeee', locale).toUpperCase()
  const date = format(now, 'dd.MM', locale)

  return (
    <div className="DailyPlan">
      <div className="DailyPlan-Title">
        <h2>Tagesplan</h2>
        <div className="DailyPlan-Date">
          <div>{day}</div>
          <div>{date}</div>
        </div>
      </div>
      <p className="DailyPlan-Demand">Täglicher Bedarf</p>
      <Nutrients cal={random()} fat={random()} prot={random()} carb={random()} />
      <Meal name="Frühstück" time="06:30 - 09:00" />
      <Recipe recipe={getRecipe('breakfast')} />
      <Meal name="Mittag" time="12:00 - 14:30" />
      <Recipe recipe={getRecipe('lunch')} />
      <Meal name="Snack" time="15:00 - 17:00" />
      <Recipe recipe={getRecipe('snack')} />
      <Meal name="Abendbrot" time="17:30 - 19:30" />
      <Recipe recipe={getRecipe('dinner')} />
    </div>
  )
}
