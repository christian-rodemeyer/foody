import { NutrientsData } from './model'
import './Nutrients.css'

export type NutrientsProps = NutrientsData

export const Nutrients = ({ cal, fat, carb, prot }: NutrientsProps) => {
  return (
    <div className="Nutrients">
      <div className="Nutrients-Circle">
        {cal}
        <br /> kcal
      </div>
      <div className="Nutrients-Circle">
        {fat}g<br /> Fett
      </div>
      <div className="Nutrients-Circle">
        {prot}g<br /> Protein
      </div>
      <div className="Nutrients-Circle">
        {carb}g<br /> Zucker
      </div>
    </div>
  )
}
