import { Literal, Record, Static, Union, Number, String, Array } from 'runtypes'
import recipes_json from './recipes.json'

const NutrientsData = Record({
  cal: Number,
  fat: Number,
  prot: Number,
  carb: Number,
})
export type NutrientsData = Static<typeof NutrientsData>

const Meal = Union(Literal('breakfast'), Literal('lunch'), Literal('snack'), Literal('dinner'))
export type Meal = Static<typeof Meal>

const RecipeData = Record({
  meal: Meal,
  image: String,
  title: String,
  subtitle: String,
  nutrients: NutrientsData,
})
export type RecipeData = Static<typeof RecipeData>

const allRecipes: RecipeData[] = Array(RecipeData).check(recipes_json)

export const getRecipe = (meal: Meal) => {
  const meals = allRecipes.filter((r) => r.meal === meal)
  return meals[Math.floor(Math.random() * meals.length)]
}
