import './Meal.css'
import { ReloadIcon } from './svg/ReloadIcon'

export type MealProps = {
  name: string
  time: string
}

export const Meal = ({ name, time }: MealProps) => {
  return (
    <div className="Meal">
      <div className="Meal-Title">{name}</div>
      <div className="Meal-Time">{time}</div>
      <div className="Meal-Reload">
        <ReloadIcon /> NEU LADEN
      </div>
    </div>
  )
}
